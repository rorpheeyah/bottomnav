package com.rorpheeyah.bottomnavlottie;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.RawRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.rorpheeyah.bottomnavlottie.databinding.ActivityMainBinding;

/**
 * @author Math Rorpheeyah
 */
public class MainActivity extends AppCompatActivity {

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.rorpheeyah.bottomnavlottie.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        // reset BottomNavigationView item views
        for (int i = 0; i < navView.getMenu().size(); i++) {

            BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
            BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(i);

            switch (i){
                case 0:
                    itemView.addView(lottieView(R.raw.home));
                    break;
                case 1:
                    itemView.addView(lottieView(R.raw.dashboard));
                    break;
                case 2:
                    itemView.addView(lottieView(R.raw.notification));
                    break;
            }
        }

        // BottomNavigationView item selection listener
        navView.setOnItemSelectedListener(item -> {
            if(NavigationUI.onNavDestinationSelected(item, navController)){

                // stop/reset others lottie animation
                for (int i = 0; i < navView.getMenu().size(); i++) {

                    BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
                    BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(i);
                    if(itemView != null){
                        LottieAnimationView lottieAnimationView = itemView.findViewById(R.id.lottie);
                        if(lottieAnimationView != null) {
                            lottieAnimationView.setProgress(0);
                            lottieAnimationView.cancelAnimation();
                        }
                    }
                }

                // play current item animation
                BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
                BottomNavigationItemView itemView = null;

                switch (item.getItemId()){
                    case R.id.navigation_home:
                        itemView = (BottomNavigationItemView) menuView.getChildAt(0);
                        break;
                    case R.id.navigation_dashboard:
                        itemView = (BottomNavigationItemView) menuView.getChildAt(1);
                        break;
                    case R.id.navigation_notifications:
                        itemView = (BottomNavigationItemView) menuView.getChildAt(2);
                        break;
                }

                if(itemView != null){
                    LottieAnimationView lottieAnimationView = itemView.findViewById(R.id.lottie);
                    if(lottieAnimationView != null && !lottieAnimationView.isAnimating()) lottieAnimationView.playAnimation();
                    return true;
                }
                return true;
            }
            return false;
        });
    }

    /**
     * Generate animated lottie view by providing Lottie JSON resource file (raw package)
     * @param lottieResource JSON resource file (located in raw package)
     * @return View with Lottie
     */
    private View lottieView(@RawRes int lottieResource){
        View view = View.inflate(this, R.layout.layout_lottie_item, null);
        if(view != null){
            LottieAnimationView lottieAnimationView = view.findViewById(R.id.lottie);
            lottieAnimationView.setAnimation(lottieResource);
            view.setPadding(0, 0, 0, 20);
        }
        return view;
    }

}